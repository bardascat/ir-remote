import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ToppingPage } from './topping.page';
import { ToppingPageRoutingModule } from './topping-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ToppingPageRoutingModule
  ],
  declarations: [ToppingPage]
})
export class ToppingPageModule {}
