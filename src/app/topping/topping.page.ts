import { Component } from '@angular/core';
import { AppConfig } from '../config/config';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-topping',
  templateUrl: 'topping.page.html',
  styleUrls: ['topping.page.scss']
})
export class ToppingPage {

  constructor(
    private http: HttpClient) { console.log(AppConfig) }

  onAction(action) {
    let api = "/event"
    let payload = {"remote":"topping", "action": action};
    let url = AppConfig.baseUrl+api;
    this.http.post<any>(url, payload).subscribe(data => {

    })

  }

}
