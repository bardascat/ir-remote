import { Component } from '@angular/core';
import { AppConfig } from '../config/config';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-tv',
  templateUrl: 'tv.page.html',
  styleUrls: ['tv.page.scss']
})
export class TvPage {

  constructor(
    private http: HttpClient) { console.log(AppConfig) }

  onAction(action) {
    let api = "/event"
    let payload = {"remote":"tv", "action": action};
    let url = AppConfig.baseUrl+api;
    this.http.post<any>(url, payload).subscribe(data => {

    })

  }

}
