import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TvPage } from './tv.page';
import { TvPageRoutingModule } from './tv-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TvPageRoutingModule
  ],
  declarations: [TvPage]
})
export class TvPageModule {}
