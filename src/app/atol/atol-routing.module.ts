import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AtolPage } from './atol.page';

const routes: Routes = [
  {
    path: '',
    component: AtolPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AtolPageRoutingModule {}
