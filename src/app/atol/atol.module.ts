import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AtolPage } from './atol.page';
import { AtolPageRoutingModule } from './atol-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    AtolPageRoutingModule
  ],
  declarations: [AtolPage]
})
export class AtolPageModule {}
