import { Component } from '@angular/core';
import { AppConfig } from '../config/config';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-atol',
  templateUrl: 'atol.page.html',
  styleUrls: ['atol.page.scss']
})
export class AtolPage {

  constructor(
    private http: HttpClient) { console.log(AppConfig) }

  onAction(action) {
    let api = "/event"
    let payload = {"remote":"atol", "action": action};
    let url = AppConfig.baseUrl+api;
    this.http.post<any>(url, payload).subscribe(data => {

    })

  }

}
